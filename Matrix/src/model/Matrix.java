package model;

/**
 * User: roman.loyko
 * Date: 4/2/13
 * Time: 5:45 PM
 */
public class Matrix {
    private float[][] matrixFloat;

    public Matrix(float[][] matrix) {
        matrixFloat = new float[matrix.length][];
        for (int i = 0; i < matrix.length; i++) {
            matrixFloat[i] = matrix[i];
        }
    }

    /**
     * Determinant is counted by the Laplas formula: det(A) = Sum(a[i][j]*A[i][j]),
     * where A[i][j] = ((-1)^(i+j))*M[i][j]),
     * where M[i][j] is a det which exponent is on 1 lower than A's (called Minor)
     * It can be calculated only for NxN arrays (square arrays)
     *
     * @return determinant
     */
    public float det() {
        if (matrixFloat.length != matrixFloat[0].length) return 0;
        if (matrixFloat.length == 1)
            return matrixFloat[0][0];
        if (matrixFloat.length == 2)
            return matrixFloat[0][0] * matrixFloat[1][1]
                   - matrixFloat[0][1] * matrixFloat[1][0];
        return calculateDet(matrixFloat);
    }

    private float calculateDet(float[][] aa) {
        if (aa.length == 3) {
            return aa[0][0] * aa[1][1] * aa[2][2]
                    + aa[0][1] * aa[1][2] * aa[2][0]
                    + aa[1][0] * aa[0][2] * aa[2][1]
                    - aa[2][0] * aa[1][1] * aa[0][2]
                    - aa[0][0] * aa[1][2] * aa[2][1]
                    - aa[1][0] * aa[0][1] * aa[2][2];
        }
        return 0;
    }

}
